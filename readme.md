# https://zhuanlan.zhihu.com/p/619612276

# https://ttsmaker.com/zh-cn

分享一个批量文本转语音工具
https://zhuanlan.zhihu.com/p/594635316

pip install edge-tts
你可以运行以下命令安装 translator:
pip install translator

edge-tts --text "Hello, world!" --write-media hello.mp3

edge-tts --list-voices
Name: af-ZA-AdriNeural Gender: Female
........
........
........
Name: zh-CN-XiaoxiaoNeural Gender: Female
Name: zh-CN-XiaoyiNeural Gender: Female
Name: zh-CN-YunjianNeural Gender: Male
Name: zh-CN-YunxiNeural Gender: Male
Name: zh-CN-YunxiaNeural Gender: Male
Name: zh-CN-YunyangNeural Gender: Male
Name: zh-CN-liaoning-XiaobeiNeural Gender: Female
Name: zh-CN-shaanxi-XiaoniNeural Gender: Female
Name: zh-HK-HiuGaaiNeural Gender: Female
Name: zh-HK-HiuMaanNeural Gender: Female
Name: zh-HK-WanLungNeural Gender: Male
Name: zh-TW-HsiaoChenNeural Gender: Female
Name: zh-TW-HsiaoYuNeural Gender: Female
Name: zh-TW-YunJheNeural Gender: Male
Name: zu-ZA-ThandoNeural Gender: Female
Name: zu-ZA-ThembaNeural Gender: Male
