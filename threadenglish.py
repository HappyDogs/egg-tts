import os 
import asyncio  
import threading
import edge_tts
from translator import BaiduTranslator

mp3_folder = 'engtohanmp3'  
voice = 'zh-CN-YunxiNeural'  

async def text_to_speech(text, filename):
    tts = edge_tts.Communicate(text=text, voice=voice)
    await tts.save(os.path.join(mp3_folder, filename + '.mp3'))

def translate_text(text):
    translator = BaiduTranslator()
    translation = translator.translate(text, 'zh')
    return translation

def main():
    # Check if MP3 folder exists, if not create it
    if not os.path.exists(mp3_folder):
        os.mkdir(mp3_folder)
        
    loop = asyncio.get_event_loop()
    files = os.listdir('engtohan/') 
    threads = []
    for i, file in enumerate(files):
        filename = str(i + 1)
        with open(os.path.join('engtohan/', file), encoding='utf-8') as f:
            text = f.read() 
            
        translated = translate_text(text)  
        
        thread = threading.Thread(target=asyncio.run, 
                                  args=(text_to_speech(translated, filename),))
        threads.append(thread)
        thread.start()
    
    # Join all threads 
    for thread in threads:
        thread.join()  
        
    # Close event loop        
    loop.close()
    
if __name__ == '__main__':  
    loop = asyncio.get_event_loop()
    main()