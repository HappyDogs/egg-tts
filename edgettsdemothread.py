
# import os
# import threading
# import edge_tts

# voice = 'zh-CN-YunxiNeural'

# def text_to_speech(text, filename):
#     tts = edge_tts.Communicate(text=text, voice=voice)
#     tts.save(filename + '.mp3')

# def main():
#     files = os.listdir('texts/')
#     for i, file in enumerate(files):
#         filename = str(i + 1)
#         with open(os.path.join('texts/', file), encoding='utf-8') as f:
#             text = f.read()
        
#         thread = threading.Thread(target=text_to_speech, args=(text, filename))
#         thread.start()

# if __name__ == '__main__':
#     main() 

""" 

import os
import asyncio
import threading
import edge_tts

voice = 'zh-CN-YunxiNeural'

async def text_to_speech(text, filename):
    tts = edge_tts.Communicate(text=text, voice=voice)
    await tts.save(filename + '.mp3')

def main():
    loop = asyncio.get_event_loop()
    files = os.listdir('texts/')
    mp3_folder = 'mp3'
    if not os.path.exists(mp3_folder):
        os.mkdir(mp3_folder)
    for i, file in enumerate(files):
        filename = str(i + 1)
        with open(os.path.join('texts/', file), encoding='utf-8') as f:
            text = f.read()
        
        tts.save(os.path.join(mp3_folder, filename + '.mp3'))
        thread = threading.Thread(target=asyncio.run, args=(text_to_speech(text, filename),))
        thread.start()

if __name__ == '__main__': 
    loop = asyncio.get_event_loop()
    main() """


import os 
import asyncio 
import threading
import edge_tts

voice = 'zh-CN-YunxiNeural'  
mp3_folder = 'mp3'

async def text_to_speech(text, filename):
    tts = edge_tts.Communicate(text=text, voice=voice)
    await tts.save(os.path.join(mp3_folder, filename + '.mp3'))

def main():
    # Check if MP3 folder exists, if not create it
    if not os.path.exists(mp3_folder):
        os.mkdir(mp3_folder)
        
    loop = asyncio.get_event_loop()
    files = os.listdir('texts/') 
    threads = []
    for i, file in enumerate(files):
        filename = str(i + 1)
        with open(os.path.join('texts/', file), encoding='utf-8') as f:
            text = f.read()
        
        thread = threading.Thread(target=asyncio.run, args=(text_to_speech(text, filename),))
        threads.append(thread)
        thread.start()
    
    # Join all threads 
    for thread in threads:
        thread.join()  
        
    # Close event loop        
    loop.close()
    
if __name__ == '__main__':  
    loop = asyncio.get_event_loop()
    main()